#include <DNSServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecureBearSSL.h>
#include "WiFiManager.h"
#include <WiFiUdp.h>

#define CONFIG_VERSION 47946304

typedef struct {
    unsigned long int version;
    char mqtt_server[51];
    char name[16];
    unsigned int lightHigh;
    unsigned int lightLow;
} config;



class StavrosUtils {
private:
    WiFiManager wifiManager;
    IPAddress broadcastIP;

    void _connectToWiFi(int timeout) {
        if (WiFi.status() == WL_CONNECTED) {
            return;
        }
        debug("Connecting to WiFi...");
        if (!loadConfig()) {
            // If the config changed, reset all our settings.
            wifiManager.resetSettings();
        }

        WiFi.forceSleepWake();
        WiFiManagerParameter custom_mqtt_server("server", "MQTT server hostname", this->configuration.mqtt_server, 50);
        wifiManager.addParameter(&custom_mqtt_server);

        WiFiManagerParameter name("name", "The name of this device", this->configuration.name, 15);
        wifiManager.addParameter(&name);

        char lightHighDefault[6];
        snprintf(lightHighDefault, sizeof(lightHighDefault), "%u", this->configuration.lightHigh);
        WiFiManagerParameter lightHigh("lightHigh", "The level of light to consider high (1-1024)", lightHighDefault, 5);
        wifiManager.addParameter(&lightHigh);

        char lightLowDefault[6];
        snprintf(lightLowDefault, sizeof(lightLowDefault), "%u", this->configuration.lightLow);
        WiFiManagerParameter lightLow("lightLow", "The level of light to consider low (1-1024)", lightLowDefault, 5);
        wifiManager.addParameter(&lightLow);

        if (timeout > 0) {
            wifiManager.setConfigPortalTimeout(timeout);
        }
        wifiManager.setHostname(PROJECT_NAME);
        wifiManager.autoConnect(PROJECT_NAME);
        this->broadcastIP = WiFi.localIP();
        this->broadcastIP[3] = 255;

        strcpy(this->configuration.mqtt_server, custom_mqtt_server.getValue());
        strcpy(this->configuration.name, name.getValue());
        this->configuration.lightHigh = atoi(lightHigh.getValue());
        this->configuration.lightLow = atoi(lightLow.getValue());

        if (this->configuration.lightHigh < this->configuration.lightLow) {
            // Swap the values around, as the user made a mistake.
            unsigned int intermediate;
            intermediate = this->configuration.lightHigh;
            this->configuration.lightHigh = this->configuration.lightLow;
            this->configuration.lightLow = intermediate;
        }
        saveConfig();
        debug("Connected to WiFi.");
    }

public:
    config configuration;

    bool loadConfig() {
        // Return true if the configuration was loaded successfully, false otherwise.
        EEPROM.begin(sizeof(this->configuration));
        EEPROM.get(0, this->configuration);
        if (this->configuration.version != CONFIG_VERSION) {
            configuration.version = CONFIG_VERSION;
            strncpy(configuration.mqtt_server, "", 50);
            snprintf(configuration.name, 50, "%08X", ESP.getChipId());
            configuration.lightHigh = 100;
            configuration.lightLow = 50;
            return false;
        }
        return true;
    }

    void saveConfig() {
        EEPROM.begin(sizeof(this->configuration));
        EEPROM.put(0, this->configuration);
        EEPROM.commit();
    }

    void logUDP(String message) {
        if (WiFi.status() != WL_CONNECTED) {
            return;
        }

        WiFiUDP Udp;

        // Listen with `nc -kul 37243`.
        Udp.beginPacket(this->broadcastIP, 37243);
        Udp.write(("(" PROJECT_NAME  ": " + String(millis()) + " - " + WiFi.localIP().toString() + ") " + ": " + message + "\n").c_str());
        Udp.endPacket();
    }

    void debug(String text) {
        Serial.println(text);
        logUDP(text);
    }

    void doHTTPUpdate() {
        if (WiFi.status() != WL_CONNECTED) {
            return;
        }
        debug("[update] Looking for an update from v" VERSION ".");

        std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
        client->setFingerprint(OTA_FINGERPRINT);
        t_httpUpdate_return ret = ESPhttpUpdate.update(*client, "https://" OTA_HOSTNAME "/" PROJECT_NAME "/", VERSION);
        switch (ret) {
        case HTTP_UPDATE_FAILED:
            debug("[update] Update failed.");
            break;
        case HTTP_UPDATE_NO_UPDATES:
            debug("[update] No update from v" VERSION ".");
            break;
        case HTTP_UPDATE_OK:
            debug("[update] Update ok.");
            break;
        }
        debug("Ready.");
    }


    void resetWiFiSettings() {
        wifiManager.resetSettings();
    }

    void connectToWiFi() {
        _connectToWiFi(0);
    }


    void connectToWiFi(int timeout) {
        _connectToWiFi(timeout);
    }
};
