#include <Bounce2.h>
#include <DHT.h>
#include <DNSServer.h>
#include <EEPROM.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFi.h>
#include "PubSubClient.h"
#include "transmit.h"
#include <WiFiClientSecureBearSSL.h>
#include <WiFiUdp.h>
#include <StavrosUtils.h>

StavrosUtils utils;

#ifndef MQTT_PORT
#define MQTT_PORT 1883
#endif

#define ACTIVE_MOTION_THRESHOLD 5 * 60
#define LIGHT_ON_THRESHOLD 50
#define LIGHT_OFF_THRESHOLD 400

#define MOTION_PIN 4
#define PRESENCE_PIN 12
#define TEMPERATURE_PIN 14
#define LED_PIN 2
#define IR_PIN 16
#define ALARM_PIN 5
#define LIGHT_PIN A0

#define MAX_MESSAGE_LENGTH 600
#define DELAY_MULTIPLIER 100

DHT dhtSensor(TEMPERATURE_PIN, DHT22);
IPAddress broadcastIP;
WiFiClient wclient;
char COMMAND_TOPIC[128];
char GROUP_TOPIC[128];
char STATE_TOPIC[128];
char LOG_TOPIC[128];
char HA_TEMP_TOPIC[128];
char HA_HUM_TOPIC[128];
char HA_MOTION_TOPIC[128];

char INSTANCE_NAME[16];

Bounce presenceDebouncer = Bounce();

PubSubClient client(wclient);

struct Sensors {
    unsigned long motion = 0;
    unsigned long presence = 0;
    unsigned int humidity = 0;
    unsigned int temperature = 0;
    unsigned int lightLevel = 0;
    bool lightBright = false;
} sensors;

struct Misc {
    unsigned int alarm = 0;
} misc;

void updateSensors(bool forcePublish);

static unsigned long lastUpdate = 0;
// Publish a message to MQTT if connected.
void mqttPublish(String topic, String payload) {
    if (!client.connected()) {
        return;
    }
    client.publish(topic.c_str(), payload.c_str());
}

// Receive a message from MQTT and act on it.
void mqttCallback(char *chTopic, byte *chPayload, unsigned int length) {
    chPayload[length] = '\0';
    String payload = String((char *)chPayload);

    if (payload[0] == 1) {
        utils.debug("Got an infrared command.");
    } else {
        utils.debug("Got a command: " + payload);
    }

    if (payload == "alarm on") {
        misc.alarm = 1;
        digitalWrite(ALARM_PIN, HIGH);
    } else if (payload == "alarm off") {
        misc.alarm = 0;
        digitalWrite(ALARM_PIN, LOW);
    } else if (payload == "update") {
        utils.debug("Got an update command.");
        updateSensors(true);
    } else if (payload == "reset") {
        utils.debug("Got a reset command, wiping all configuration...");
        delay(500);
        utils.resetWiFiSettings();
        ESP.restart();
    } else if (payload == "reboot") {
        utils.debug("Got a reboot command, rebooting...");
        delay(500);
        ESP.restart();
    } else if (payload[0] == 1) {
        utils.debug("Sending an infrared command.");
        // We need to copy the array here, as it will go away.
        byte* mPayload = new byte[length];
        memcpy(mPayload, chPayload, length);
        cmdSend(mPayload[3], &mPayload[6], mPayload[4], mPayload[5] * DELAY_MULTIPLIER, mPayload[1] - 1, mPayload[2] - 1);
        delete[] mPayload;
    }
}

// Send a command, printing some debug information.
void cmdSend(char pin, unsigned char message[], unsigned char repeat, unsigned int intraDelay, unsigned char dcHigh, unsigned char dcLow) {
    unsigned int out = 0;
    unsigned int i;

    Serial.print("Sending on pin ");
    Serial.print(pin, DEC);
    Serial.print(", repeating ");
    Serial.print(repeat, DEC);
    Serial.print(" times, for ");
    Serial.print(intraDelay);
    Serial.print(" with a high duty cycle of ");
    Serial.print(dcHigh, DEC);
    Serial.print(" and a low duty cycle of ");
    Serial.print(dcLow, DEC);
    Serial.println(".");

    // Print the received timings to the console.
    for (i = 0; i < MAX_MESSAGE_LENGTH; i = i + 2) {
        if (message[i] == 0 || message[i + 1] == 0) {
            break;
        }
        out = (message[i] << 8) + message[i + 1];
        Serial.print(out, DEC);
        Serial.print(" ");
    }
    Serial.println("");
    yield();

    transmit(IR_PIN, message, repeat, intraDelay, dcHigh, dcLow);

    utils.debug("Command sent.");
}

void resetPins() {
    pinMode(1, OUTPUT);
    digitalWrite(1, LOW);
    pinMode(2, OUTPUT);
    digitalWrite(2, LOW);
    pinMode(3, OUTPUT);
    digitalWrite(3, LOW);
    pinMode(4, OUTPUT);
    digitalWrite(4, LOW);
    pinMode(5, OUTPUT);
    digitalWrite(5, LOW);
    pinMode(12, OUTPUT);
    digitalWrite(12, LOW);
    pinMode(13, OUTPUT);
    digitalWrite(13, LOW);
    pinMode(14, OUTPUT);
    digitalWrite(14, LOW);
    pinMode(15, OUTPUT);
    digitalWrite(15, LOW);
    pinMode(16, OUTPUT);
    digitalWrite(16, LOW);
}

// Return a JSON string of the entire state of the inputs.
String sensorState() {
    return String(String("{\n") +
                  "\"MOTION\": " + (sensors.motion == 1 ? "true" : "false") + ",\n" +
                  "\"PRESENCE\": " + (sensors.presence == 1 ? "true" : "false") + ",\n" +
                  "\"OCCUPANCY\": " + ((sensors.presence == 1 || sensors.motion == 1) ? "true" : "false") + ",\n" +
                  "\"ALARM\": " + String(misc.alarm, DEC) + ",\n" +
                  "\"HUMIDITY\": " + String(sensors.humidity, DEC) + ",\n" +
                  "\"TEMPERATURE\": " + String(sensors.temperature, DEC) + ",\n" +
                  "\"LIGHT_LEVEL\": " + String(sensors.lightLevel, DEC) + ",\n" +
                  "\"LIGHT_BRIGHT\": " + String(sensors.lightBright ? "true" : "false") + "\n" +
                  "}\n");
}


// Update the sensors to their latest values.
void updateSensors(bool forcePublish = false) {
    char motionInput = 0;
    char presenceInput = 0;
    unsigned int humidity, temperature;
    bool publish = false;

    presenceDebouncer.update();

    motionInput = digitalRead(MOTION_PIN);
    presenceInput = presenceDebouncer.read();

    if (motionInput != sensors.motion || presenceInput != sensors.presence) {
        publish = true;
    }

    sensors.presence = presenceInput;
    sensors.motion = motionInput;
    sensors.lightLevel = analogRead(LIGHT_PIN);

    if (sensors.lightBright == false && (sensors.lightLevel > utils.configuration.lightHigh)) {
        sensors.lightBright = true;
        publish = true;
    } else if (sensors.lightBright == true && (sensors.lightLevel < utils.configuration.lightLow)) {
        sensors.lightBright = false;
        publish = true;
    }

    // We don't need to read this every update interval.
    if (millis() % 50 == 0) {
        humidity = dhtSensor.readHumidity();
        temperature = dhtSensor.readTemperature();

        // The sensor sometimes glitches and reads MAXINT, work around that.
        if (humidity <= 100 && temperature < 100) {
            if (sensors.humidity != humidity || sensors.temperature != temperature) {
                publish = true;
            }
            sensors.humidity = humidity;
            sensors.temperature = temperature;
        }
    }

    if (publish || forcePublish) {
        publishState();
    }
}

// Publish our current sensor readings to MQTT.
void publishState() {
    mqttPublish(STATE_TOPIC, sensorState());
}

void setupMQTT() {
    client.setBufferSize(2 * 1024);
    client.setServer(utils.configuration.mqtt_server, MQTT_PORT);
    client.setCallback(mqttCallback);
}

// Check the MQTT connection and reboot if we can't connect.
void connectMQTT() {
    client.loop();

    if (client.connected()) {
        return;
    }
    utils.debug("Client not connected.");

    int retries = 4;
    String mac = WiFi.macAddress();
    utils.debug("Connecting to MQTT " + String(utils.configuration.mqtt_server) + ", MAC is " + mac + "...");
    while (!client.connect(mac.c_str()) && retries--) {
        delay(500);
        utils.debug("Retry...");
    }

    if (!client.connected()) {
        utils.debug("\nfatal: MQTT server connection failed. Rebooting.");
        delay(500);
        ESP.restart();
    }

    utils.debug("Connected to MQTT.");
    mqttPublish(LOG_TOPIC, "Connected, version " VERSION ".");
    client.subscribe(COMMAND_TOPIC);
    client.subscribe(GROUP_TOPIC);

    updateSensors(true);
}

void setup() {
    resetPins();
    Serial.begin(115200);
    EEPROM.begin(256);

    pinMode(MOTION_PIN, INPUT);
    pinMode(PRESENCE_PIN, INPUT);
    pinMode(LIGHT_PIN, INPUT);
    pinMode(LED_PIN, OUTPUT);
    pinMode(IR_PIN, OUTPUT);
    pinMode(ALARM_PIN, OUTPUT);
    digitalWrite(LED_PIN, LOW);
    digitalWrite(IR_PIN, LOW);
    digitalWrite(ALARM_PIN, LOW);

    // The presence sensor is prone to spurious wakeups, so we debounce it.
    presenceDebouncer.attach(PRESENCE_PIN,  INPUT);
    presenceDebouncer.interval(2000);

    dhtSensor.begin();

    utils.connectToWiFi(5 * 60);
    utils.doHTTPUpdate();

    setupMQTT();
    connectMQTT();

    digitalWrite(LED_PIN, HIGH);

    snprintf(COMMAND_TOPIC, 128, "esplights/%s/command", utils.configuration.name);
    snprintf(GROUP_TOPIC, 128, "esplights/all/command");
    snprintf(STATE_TOPIC, 128, "esplights/%s/state", utils.configuration.name);
    snprintf(LOG_TOPIC, 128, "esplights/%s/log", utils.configuration.name);
    snprintf(HA_TEMP_TOPIC, 128, "homeassistant/sensor/%s_temp/config", utils.configuration.name);
    snprintf(HA_HUM_TOPIC, 128, "homeassistant/sensor/%s_hum/config", utils.configuration.name);
    snprintf(HA_MOTION_TOPIC, 128, "homeassistant/binary_sensor/%s_motion/config", utils.configuration.name);

    snprintf(INSTANCE_NAME, 16, "%s", utils.configuration.name);

    mqttPublish(LOG_TOPIC, "Command sent.");
    mqttPublish(HA_TEMP_TOPIC,
                String("{\"device_class\": \"temperature\", \"name\": \"Temperature\", \"state_topic\": \"") +
                String(STATE_TOPIC) +
                String("\", \"unique_id\": \"esplights_") +
                String(utils.configuration.name) +
                String("_temperature\", \"unit_of_measurement\": \"°C\", \"value_template\": \"{{ value_json.TEMPERATURE }}\"}"));
    mqttPublish(HA_HUM_TOPIC,
                String("{\"device_class\": \"humidity\", \"name\": \"Humidity\", \"state_topic\": \"") +
                String(STATE_TOPIC) +
                String("\", \"unique_id\": \"esplights_") +
                String(utils.configuration.name) +
                String("_humidity\", \"unit_of_measurement\": \"%\", \"value_template\": \"{{ value_json.HUMIDITY }}\"}"));
    mqttPublish(HA_MOTION_TOPIC,
                String("{\"device_class\": \"motion\", \"name\": \"Motion\", \"state_topic\": \"") +
                String(STATE_TOPIC) +
                String("\", \"unique_id\": \"esplights_") +
                String(utils.configuration.name) +
                String("_motion\", \"value_template\": \"{{ value_json.MOTION }}\"}"));
}

void loop() {
    unsigned long currentMillis = millis();
    utils.connectToWiFi(5 * 60);
    connectMQTT();

    if (currentMillis - lastUpdate >= 60000) {
        updateSensors(true);
        lastUpdate = currentMillis;
    } else {
        updateSensors();
    }

    if (WiFi.status() != WL_CONNECTED) {
        utils.debug("Not connected to WiFi. Rebooting...");
        delay(500);
        ESP.restart();
    }
    delay(1);
}
