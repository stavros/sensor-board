Sensor board
============

This is an ESP8266-based sensor board.
It includes:

* Motion sensor.
* Presence sensor (millineter wave).
* Temperature/humidity sensor.
* Light sensor.
* Infrared LED (transmitter).
* Generic 5V switchable supply.


## Images

Here are some images of the PCB. The SVGs are the most up to date:

![](misc/sensor_board.png)
![](misc/sensor_board-top.svg)
![](misc/sensor_board-bottom.svg)
